# Trabajo Practico

## Grupo 8

IT Manager (Administración de Redes, Comunicaciones y Sistemas Operativos)

### Miembros

- Felipe, Díaz  Posse, **84056**, `felipediazposse123@gmail.com`
- Franco Manuel, Belbruno, **81165**, `francobelbruno1@gmail.com`
- Genaro Rafael, Bergesio, **83464**, `genarobergesio@gmail.com`
- José Ignacio, Martinoli Marchionni, **84281**, `ignamartinoli@proton.me`
- Marcelo Tomás, Cuello, **83296**, `tomarcuello@gmail.com`
- Octavio Félix, Cavalleris Malanca, **90052**, `octaviocavalleris@gmail.com`

## Uso

### Clonar

```sh
git clone https://gitlab.com/ignamartinoli/are

```

### Actualizar

```sh
git add .
git commit -m 'Actualización'
git push origin main
```
